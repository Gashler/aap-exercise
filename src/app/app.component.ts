import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { NotificationsService } from 'angular2-notifications';
import { Options } from 'angular2-notifications';
import { Pet } from './models/pet.model';
import { PetService } from './services/pet.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public form: any;
    public loading: boolean = false;
    public ageSort: string;
    public ageSorts: any = [
        {
            key: 'asc',
            name: 'Youngest to oldest'
        },
        {
            key: 'desc',
            name: 'Oldest to youngest'
        },
    ];
    public endNumber = 10;
    public locationRange: number;
    public locationRanges: any = [
        25,50,100,200
    ];
    public notifcationOptions: Options = {
        position: ['top', 'right'],
        clickToClose: true,
        timeOut: 2500,
        animate: 'scale',
        showProgressBar: true,
        pauseOnHover: true,
    };
    public perPage = 10;
    public petTypes: any = [
        {
            key: 'dog',
            name: 'Dogs'
        },
        {
            key: 'cat',
            name: 'Cats'
        }
    ];
    public pets: Pet[];
    public recordCount: string;
    public requestMade: boolean = false;
    public showError: boolean = false;
    public showOffers: boolean = false;
    public startNumber = 1;
    public title = 'ui';

    constructor(
        private notify: NotificationsService,
        private petService: PetService
    ) {}

    public ngOnInit(): void {
        this.form = {
            location: '',
            locationRange: this.locationRanges[0],
            type: this.petTypes[0].key,
        };
        this.ageSort = this.ageSorts[0].key;
    }

    /**
    * Request Featured Pets
    */
    public searchForPets(clear?: boolean) {
        this.loading = true;
        this.requestMade = true;
        let params = `city_or_zip=${this.form.location}&geo_range=${this.form.locationRange}&species=${this.form.type}&start_number=${this.startNumber}&end_number=${this.endNumber}`;
        this.petService
            .search(params)
            .subscribe((response: any) => {
                this.loading = false;
                this.recordCount = response.message;
                if (clear || !this.pets) {
                    this.pets = [];
                    this.pets = response.body.pets;
                    this.sortResults();
                } else {
                    for (let pet of response.body.pets) {
                        this.pets.push(pet);
                    }
                }

                // enable continuous scroll
                window.onscroll = function(ev) {
                    if (!this.loading) {
                        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                            this.startNumber += this.perPage;
                            this.endNumber += this.perPage;
                            this.searchForPets();
                        }
                    }
                }.bind(this);
            },(error: any) => {
                this.loading = false;
                this.showError = true;
                // this.notify.error('Error', 'Could not find any matching results.');
                setTimeout(function() {
                    this.showError = false;
                }.bind(this), 3000);
            });
    }

    /**
    * Sort Results
    */
    public sortResults() {
        this.pets = _.orderBy(this.pets, ['ageIndex'], [this.ageSort]);
    }
}
