import { BrowserModule } from '@angular/platform-browser';
import { BrowserXhr, HttpModule, Http } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { AAP_ENV } from '../app/models/environment.model';
import { HttpService } from '../app/services/http.service';
import { PetService } from '../app/services/pet.service';
import { Router, RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SimpleNotificationsModule.forRoot(),
  ],
  providers: [
      {
          provide: AAP_ENV,
          useValue: environment
      },
      {
          provide:  HttpService,
          useClass: HttpService,
          deps:     [Http],
      },
      {
          provide:  PetService,
          useClass: PetService,
          deps:     [HttpService, AAP_ENV],
      },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
