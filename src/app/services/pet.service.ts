import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { AAP_ENV, Environment } from '../models/environment.model';
import { HttpService } from './http.service';
import { Pet } from './../models/pet.model';

@Injectable()
export class PetService {
    public omit: Array<string> = ['created_at', 'updated_at'];

    constructor(
        protected http: HttpService,
        @Inject(AAP_ENV) protected environment: Environment
    ) {}

    /**
    * Request featured pets.
    */
    public search(params: any = []): Observable<any> {
        return this.http
        .hostname(this.environment.api_url)
        .get(`pets/search?${params}`);
    }
}
