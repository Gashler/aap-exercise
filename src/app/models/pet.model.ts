export class Pet {
    public age: string;
    public ageIndex?: number;
    public breeds: any;
    public clanId: number;
    public colorId: number;
    public createdDate: string;
    public media: any;
    public name: string;
    public petId: number;
    public radius: number;
    public sex: string;
    public shelterId: number;
    public shots: any;
    public sizeId: number;
    public specialNeeds: any;
    public bondedTo: any;
    public city: string;
    public state: string;
    public actQuickly: boolean;
    public stateName: string
}
