import { InjectionToken } from '@angular/core';

export const AAP_ENV: InjectionToken<Environment> = new InjectionToken('aap.environment.configuration');

export class Environment {
    public api_url: string;
    public production: boolean;
}
